# Elevator

Elevator is a puzzle-game made for the xkcd-gamejam. It is based on [xkcd 897](https://xkcd.com/897/).

![](https://img.itch.zone/aW1nLzkxNTg0OS5qcGc=/original/T8PPx0.jpg)

[Download Game](https://devsaur.itch.io/elevator)